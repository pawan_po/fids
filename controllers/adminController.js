const Post = require('../models/PostModel');
const PostD = require('../models/PostDModel');
const Category = require('../models/CategoryModel');

const Cancle = require('../models/Cancled');
const {isEmpty} = require('../config/customFunctions');
const User = require('../models/UserModel');

const mongoose = require('mongoose');
// const io = require('socket.io');


module.exports = {

    index: (req, res) => {

        // console.log(req.app.get('io'));
        const id = req.params.id;

        Post.find()
        .populate('category')
        .exec()
        .then(posts =>{
            PostD.find()
            .populate('category')
            .exec()
            .then(postd=>{


                Category.find().then(cat =>{

                    res.render('admin/index', {posts: posts, postd: postd, cat: cat});
    
                });
    

            });

        });

    },


    /* ADMIN POSTS ENDPOINTS */


    getPosts: (req, res) => {
        Post.find()
            .populate('category')
            .exec()
            .then(posts => {
                PostD.find()
                .populate('category')
            .exec()
                .then(postd=>{

                    res.render('admin/posts/index', {posts: posts, postd : postd});


                });
            });
    },


    createPostsGet: (req, res) => {
        Category.find().then(cats => {

            res.render('admin/posts/create', {categories: cats});
        });


    },


    createDPostsGet: (req, res) => {
        Category.find().then(cats => {

            res.render('admin/posts/createD', {categories: cats});
        });


    },

    submitPosts: (req, res) => {


        // Check for any input file
        let filename = '';
        var io = req.app.get('io');

       if(!isEmpty(req.files)) {
           let file = req.files.uploadedFile;
           filename = file.name;
           let uploadDir = './public/uploads/';
           
           file.mv(uploadDir+filename, (err) => {
               if (err)
                   throw err;
           });
       }
        
        const newPost = new Post({


            title: req.body.title,
            Flight: req.body.Flight,
            Sched_Time: req.body.Sched_Time,
            Actual_Time: req.body.Actual_Time,
            // Destination: req.body.Destination,
            Check_In: req.body.Check_In,
            Gate: req.body.Gate,
            status: req.body.status,
            category: req.body.category,

            file: `/uploads/${filename}`
        });

        newPost.save().then(post => {
            req.flash('success-message', 'Post created successfully.');
            res.redirect('/admin');
        });

         io.emit('message', newPost);

    },

    submitDPosts: (req, res) => {

 
        // Check for any input file
        let filename = '';
        var io = req.app.get('io');
        
       if(!isEmpty(req.files)) {
           let file = req.files.uploadedFile;
           filename = file.name;
           let uploadDir = './public/uploads/';
           
           file.mv(uploadDir+filename, (err) => {
               if (err)
                   throw err;
           });
       }
        
        const newPostD = new PostD({

            title: req.body.title,
            Flight: req.body.Flight,
            Sched_Time: req.body.Sched_Time,
            Actual_Time: req.body.Actual_Time,
            // Destination: req.body.Destination,
            Check_In: req.body.Check_In,
            Gate: req.body.Gate,
            status: req.body.status,
            category: req.body.category,
            file: `/uploads/${filename}`
        });

        newPostD.save().then(post => {
            req.flash('success-message', 'Post created successfully.');
            res.redirect('/admin');
        });


        io.emit('messaged', newPostD);


    },


    editPostGetRoute: (req, res) => {
       const id = req.params.id;
        // var id = mongoose.Types.ObjectId.isValid(req.params.id);
        

        
        Post.findById(id)
            .then(post => {
                Category.find().then(categories=>{
                
                    res.render('admin/posts/edit', {post : post, categories:categories});

                })
                

                // Category.find().then(cats => {
                    
                // });


            });


         
    },



    editPostUpdateRoute: (req, res) => {
        let filename = '';

        const id = req.params.id;
        var io = req.app.get('io');

        if(!isEmpty(req.files)) {
            let file = req.files.uploadedFile;
            filename = file.name;
            let uploadDir = './public/uploads/';
            
            file.mv(uploadDir+filename, (err) => {
                if (err)
                    throw err;
            });
        }

        var newPost=Post.findById(id)
            .then(post => {

                post.title = req.body.title;
                post.Flight = req.body.Flight;
                post.Sched_Time= req.body.Sched_Time,
                post.Actual_Time= req.body.Actual_Time,
                // post.Destination= req.body.Destination,
                post.Check_In= req.body.Check_In,
                post.Gate= req.body.Gate,
                post.status= req.body.status,
                post.category = req.body.category;
                file: `/uploads/${filename}`

                // post.status = req.body.status;
                // post.allowComments = req.body.allowComments;
                // post.description = req.body.description;
                // post.category = req.body.category;


           
                post.save().then(updatePost => {
                    req.flash('success-message', `The Post ${updatePost.title} has been updated.`);
                    res.redirect('/admin');
                    io.emit('messageedit', updatePost);

                });
            });



    },

    editPostGetRouteD: (req, res) => {
        const id = req.params.id;
         // var id = mongoose.Types.ObjectId.isValid(req.params.id);
         
         PostD.findById(id)
             .then(post => {
                 
 
                  Category.find().then(categories => {
                     res.render('admin/posts/editd', {post : post, categories:categories});
                  });
 
 
             });
    
     },

     editPostUpdateRouteD: (req, res) => {

        const id = req.params.id;
        var io = req.app.get('io');


        PostD.findById(id)
            .then(post => {

                post.title = req.body.title;
                post.Flight = req.body.Flight;
                post.Sched_Time= req.body.Sched_Time,
                post.Actual_Time= req.body.Actual_Time,
                // post.Destination= req.body.Destination,
                post.Check_In= req.body.Check_In,
                post.Gate= req.body.Gate,
                post.status= req.body.status,
                post.category = req.body.category;


           
                post.save().then(updatePost => {
                    req.flash('success-message', `The Post ${updatePost.title} has been updated.`);
                    res.redirect('/admin');
                    io.emit('messageeditd', updatePost);


                });
            });

    },


    deletePost: (req, res) => {

        var io = req.app.get('io');
        var id = req.params.id;

       const newPost = Post.findByIdAndDelete(req.params.id)
            .then(deletedPost => {
                PostD.findByIdAndDelete(req.params.id).then(deletedPostD =>{

                    req.flash('success-message');
                res.redirect('/admin');

                
                // io.emit('deletemessage', deletedPost);
                })
                
            });



            io.emit('deletemessage', id);
            io.emit('deletemessaged', id);

           

    },



    /* ALL CATEGORY METHODS*/
    getCategories: (req, res) => {

        Category.find().then(cats => {
            res.render('admin/category/index', {categories: cats});
        });
    },

    createCategories: (req, res) => {
        var categoryName = req.body.name;

        if (categoryName) {
            const newCategory = new Category({
                title: categoryName
            });

            newCategory.save().then(category => {
                res.status(200).json(category);
            });
        }

    },

    editCategoriesGetRoute: async (req, res) => {
        const catId = req.params.id;

        const cats = await Category.find();


        Category.findById(catId).then(cat => {

            res.render('admin/category/edit', {category: cat, categories: cats});

        });
    },


    editCategoriesPostRoute: (req, res) => {
        const catId = req.params.id;
        const newTitle = req.body.name;

        if (newTitle) {
            Category.findById(catId).then(category => {

                category.title = newTitle;

                category.save().then(updated => {
                    res.status(200).json({url: '/admin/category'});
                });

            });
        }
    },



    deleteCatPost: (req, res) => {

        var io = req.app.get('io');
        var id = req.params.id;

       const newPost = Category.findByIdAndDelete(req.params.id)
            .then(deletedPost => {
                    req.flash('success-message');
                res.redirect('/admin/category');
                
                // io.emit('deletemessage', deletedPost);                
            
            });

           

    },

    getCancle: (req, res) => {

            res.render('admin/category/cancled');
       
    },
    
    submitCancle: (req, res) => {

    
        const cancle = new Cancle({

            title: req.body.title,
            Flight: req.body.Flight,
            reason: req.body.reason
        });

        cancle.save().then(post => {
            req.flash('success-message', 'Post created successfully.');
            res.redirect('/admin/posts/cancled');
        });


    },


    indexcancle: (req, res) => {

        // console.log(req.app.get('io'));

        

                Cancle.find().then(cancle =>{

                    res.render('admin/category/cancleind', {cancle:cancle});
    
                });
    

          

    },





};    
    
