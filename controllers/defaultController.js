
const Post = require('../models/PostModel');
const PostD = require('../models/PostDModel');
const User = require('../models/UserModel');
const Category = require('../models/CategoryModel')


const bcrypt = require('bcryptjs'); 


module.exports = {


    index: async (req, res)=>{

        req.app.get('io').emit('message', 'messahedsfdf');
        req.app.get('io').emit('messageedit', 'messahedsfdfhg');
        
       const posts = await Post.find()
       .populate('category')
       .exec();
       const categories = await Category.find();
       const user = await User.find();

       res.render('default/index', {posts: posts, categories: categories, user: user});  

       
    
    },
    dep: async (req, res)=>{

        req.app.get('io').emit('messaged', 'messahedsfdfbn');
        req.app.get('io').emit('messageeditd', 'messahedsfdfhg');
        
       const postd = await PostD.find()
       .populate('category')
       .exec();
       const categories = await Category.find();
       const user = await User.find();

       res.render('default/departure', {postd: postd, categories: categories, user: user});  

       
    
    },

    loginGet: (req, res)=>{
        res.render('default/login');



    },
    loginPost: (req, res)=>{

        res.send("congo data submitted sucess");

    },

    registerGet: (req, res)=>{

        res.render('default/register');

    },
    registerPost: (req, res)=>{

        let errors = [];
        //validation
        if(!req.body.firstName){

            errors.push({message: 'First name is mandatory'});
        }
        if(!req.body.lastName){

            errors.push({message: 'Last name is mandatory'});
        }
        if(!req.body.email){

            errors.push({message: 'Email is mandatory'});
        }
        if(req.body.password !== req.body.passwordConfirm){

            errors.push({message: 'Passwords donot match'});
        }

        if(errors.length>0){
            res.render('default/register', {
                errors: errors,
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email

            });
        }

        else{

        
            User.findOne({email: req.body.email}).then( user =>{
                if(user){
                    req.flash('error-message', 'Email already Exists, try to Login');
                    res.redirect('/login');
                }else{
                    const newUser = new User(req.body);
                    bcrypt.genSalt(10, (err, salt)=>{
                        bcrypt.hash(newUser.password, salt, (err, hash)=>{
                            newUser.password = hash;
                            newUser.save().then(user =>{
                                req.flash('success-message', ' You are now Registered');
                                res.redirect('/login');
                            });
                        }); 
                    });
                }
            })
        }
        

    },

    singlePost: (req, res)=>{
        const id = req.params.id;
        
        Post.findById(id).then( post =>{
            User.findById(id).then(user =>{


                if (!post) {
                    errors.email = "User not found";
                    res.status(404).json({ errors });
                    // stop further execution in this callback
                    return;
                  
                }else{
    
                   //res.status(200).json(post);
                    res.render('default/singlePost',{post: post, user: user});
    
                }
    

            });
           
        });
    }
}