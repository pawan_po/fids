
const {globalvariables} = require('./config/configuration');
const express = require('express');
var socket = require('socket.io');
const mongoose = require('mongoose');
const path = require('path');
const hbs = require('express-handlebars');
const {mongoDbUrl, PORT}= require('./config/configuration');
const flash = require('connect-flash');
const session = require('express-session');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const passport = require('passport');
const {selectOption} = require('./config/customFunctions');
const fileUpload = require('express-fileupload');



const app = express();
var http = require('http').Server(app);
const io = require('socket.io')(http);

//configure mongodb

mongoose.connect(mongoDbUrl, { useNewUrlParser: true } )
    .then( response =>{

        console.log("mongodb connected succssfully");
    }

    ).catch( err =>{
        console.log("database connection failed");
    });

    app.use(express.json());
    app.use(bodyParser.json())
    app.use(express.urlencoded({ extended: true}));
    app.use(express.static(path.join(__dirname, 'public')));

    //socket setup
    
    //flash and session


app.use(session({
    secret: 'anysecret',
    saveUninitialized: true,
    resave: true

}));

app.use(passport.initialize());
app.use(passport.session());


app.use(flash());
app.use(globalvariables);

app.use(fileUpload());

//view engine and handlebars

app.engine('handlebars', hbs({defaultLayout: 'default', helpers: {select: selectOption}}));
app.set('view engine', 'handlebars');
app.use(methodOverride('newMethod'));


//routes
const defaultRouts = require('./routes/defaultRoutes');
const adminRouts = require('./routes/adminRoutes');
app.use('/', defaultRouts);
app.use('/admin', adminRouts);
app.set('io', io);



io.sockets.on('connection', () =>{
    console.log('a user is connected');
    // io.emit('message', {'a': 'sdfsdff'}) ;
    // io.emit('broadcast', /* */);
  });


var server = http.listen(PORT, ()=>{

    console.log(`server is running in port ${PORT}`);

    // io.emit('message' ,'messages ');
});

