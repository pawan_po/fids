const express = require('express');
const router = express.Router();
const adminController = require('../controllers/adminController');
const {isUserAuthenticated} = require('../config/customFunctions');

router.all('/*', isUserAuthenticated,(req, res, next) => {
    
    req.app.locals.layout = 'admin';
    
    next();
});

/* DEFAULT ADMIN INDEX ROUTE*/

router.route('/')
    .get(adminController.index);


/* VARIOUS ADMIN POST ENDPOINTS */

router.route('/posts')
    .get(adminController.getPosts);
    


router.route('/posts/create')
    .get(adminController.createPostsGet)
    .post(adminController.submitPosts);

 router.route('/posts/created')
    .get(adminController.createDPostsGet)
    .post(adminController.submitDPosts);


router.route('/posts/edit/:id')
    .get(adminController.editPostGetRoute)
    .put(adminController.editPostUpdateRoute);

 router.route('/posts/editd/:id')
 
    .get(adminController.editPostGetRouteD)
    .put(adminController.editPostUpdateRouteD);


router.route('/posts/delete/:id')
    .delete(adminController.deletePost);


/* ADMIN CATEGORY ROUTES*/

router.route('/category')
    .get(adminController.getCategories);


router.route('/category/create')
    .post(adminController.createCategories);
    


router.route('/category/edit/:id')
    .get(adminController.editCategoriesGetRoute)
    .post(adminController.editCategoriesPostRoute);

     router.get('/logout', async (req, res) => {
        await req.logout();
        req.session.destroy();
        res.clearCookie("test")
        res.clearCookie("test.sig")
        return res.redirect('/')
      });
    
      router.route('/category/:id')
      .delete(adminController.deleteCatPost);

router.route('/posts/cancle')
      .get(adminController.indexcancle);
  
      router.route('/posts/cancled')
      .get(adminController.getCancle)
      .post(adminController.submitCancle);
  

module.exports = router;

