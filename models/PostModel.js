const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = new Schema({
    
    title: {
        type: String,
        //required: true
    },


    Flight: {
        type: String,
        //required: true
    },
    Sched_Time: {
        type: String,
        //required: true
    },
    
    Actual_Time: {
        type: String,
        //required: true
    },
    Destination: {
        type: String,
        //required: true
    },
    Check_In: {
        type: String,
        //required: true
    },
    Gate: {
        type: String,
        //required: true
    },
    
    status: {
        type: String,
        //default: 'public'
    },
    
    
    creationDate: {
        type: Date,
        default: Date.now()
    },
    
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    
    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category'
    },
    
    file: {
        type: String,
        default: ''
    }
    
    
});

module.exports =  mongoose.model('Post', PostSchema );
